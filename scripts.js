const mobileNav = document.querySelector('.right-column');
const hamburgerIcon = document.getElementsByClassName('menu-logo')[0];
const closeIcon = document.querySelector('.close-icon');
const navItems = Array.from(document.querySelectorAll('.nav-link'));
const body = document.body;

//console.log(mobileNav);
/*console.log(navItems);
 */
const closeMobileNav = e => {
  mobileNav.style.display = "none";
  hamburgerIcon.style.display = "block";
  closeIcon.style.display = "none";
  /*console.log(e);
  mobileNav.classList.add('hide');
  hamburgerIcon.classList.remove('hide');
  closeIcon.classList.add('hide'); */
  // body.style.overflowY = "visible";
  /*body.classList.remove('hide-yscroll'); */ 
};

const openMobileNav = e => {
  /*mobileNav.classList.remove('hide'); */ 
    mobileNav.style.display = "block";
 /* hamburgerIcon.classList.add('hide'); */
   hamburgerIcon.style.display = "none";
  /*closeIcon.classList.remove('hide'); */
  closeIcon.style.display = "block";
  // body.style.overflowY = "hidden";
/*  body.classList.add('hide-yscroll'); */
};

hamburgerIcon.addEventListener('click', openMobileNav);

closeIcon.addEventListener('click', closeMobileNav); 
/*
mobileNav.addEventListener('click', e => {
  if (e.target.nodeName.toLowerCase() === 'a') {
    closeMobileNav();
  }
}); */

/*console.log(hamburgerIcon); */

//When nav a tag has been clicked
//the body must have a vertical scroll
// navItems.forEach(nav => {
//   nav.addEventListener('click', closeMobileNav);
//   nav.addEventListener('keypress', closeMobileNav);
// });

function myFunction(x) {
  if (x.matches) { // If media query matches
    hamburgerIcon.style.display = "block";
    closeIcon.style.display = "none";
    mobileNav.style.display = "none";
  } 
  else {
    hamburgerIcon.style.display = "none";
    closeIcon.style.display = "none";
    mobileNav.style.display = "block";
  }
}

var x = window.matchMedia("(max-width: 700px)")
myFunction(x) // Call listener function at run time
x.addListener(myFunction)